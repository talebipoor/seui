/**
 * Created by Saghar on 12/24/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ResultDialog from './ResultDialog'

const styles ={
    card: {
        margin : 10,
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
};

class CreatedPoll extends React.Component {

    render(){
        return(
            <Card className={this.props.classes.card}>
                <CardContent>
                    <Typography classname={this.props.classes.pos} variant="h5" component="h2">
                        {this.props.poll.name}
                    </Typography>
                    <Typography className={this.props.classes.pos} color="textSecondary">
                        {this.props.poll.description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <CardActions>
                        <ResultDialog pollId={this.props.poll.id}/>
                    </CardActions>
                </CardActions>
            </Card>
        );
    }
}

CreatedPoll.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreatedPoll);
