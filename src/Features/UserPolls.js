import React, { Component } from 'react';
import CenteredGrid from "../Features/CenteredGrid"
import withStyles from '@material-ui/core/styles/withStyles';
import axios from 'axios';
import Cookies from 'js-cookie';


const styles = theme => ({
    main: {
    }
});

class UserPolls extends Component {
    constructor(props) {
        super(props);
        this.state= {
            polls:{}
        }
        ;
    }

    componentWillMount(){
        this.getUserPolls();
    }

    getUserPolls(){
        var polls = {
            createdPolls:[],
            invitedPolls:[]
        }
        axios.get('http://localhost:8000/polls/polls',{
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    alert(JSON.stringify(data))
                    polls = data
                    this.setState({
                        polls:polls
                    });
                }
            })
        this.setState({
            polls:polls
        });



    }

    render(){
        return (
            <CenteredGrid polls={this.state.polls}/>
        );
    }
}

export default  withStyles(styles)(UserPolls);
