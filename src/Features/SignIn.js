import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import axios from 'axios';
import Cookies from 'js-cookie';
import { Redirect } from 'react-router-dom'


const styles = theme => ({
    "@global": {
        body: {
            // backgroundImage: "url('../bg.png')",
            // backgroundRepeat: "no-repeat",
        },
    },
        main: {
            width: 'auto',
            display: 'block', // Fix IE 11 issue.
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            marginTop: theme.spacing.unit * 3,
            [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
                width: 400,
                marginLeft: 'auto',
                marginRight: 'auto',
            },
        },
        paper: {
            marginTop: theme.spacing.unit * 30,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
        },
        avatar: {
            margin: theme.spacing.unit,
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing.unit,
            padding: theme.spacing.unit*3
        },
        submit: {
            marginTop: theme.spacing.unit * 3,
        }
});

class SignIn extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            email: '',
            redirect: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(){
        alert("submit")
        axios.get('http://localhost:8000/polls/login',{
            params:{
                username:this.state.name,
                email:this.state.email
            },
            headers:{
                "user":"username:ali"
            },
            withCredentials: true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }
                else{
                    const data = res.data;
                    Cookies.set('username',this.state.name)
                }
            })
        this.setRedirect()
    }

    setRedirect = () => {
        this.setState({
            redirect:true
        })
    }

    renderRedirect = () => {
        if(this.state.redirect){
            return <Redirect to={"/polls"}/>
        }
    }

    render(props){
        if(this.state.redirect){
            return(<Redirect to={"/polls"}/>)
        }
        else{
            return(
                <main className={this.props.classes.main}>
                    <CssBaseline />
                    <Paper className={this.props.classes.paper}>
                        {this.renderRedirect()}
                        <Typography align="center" component="h1" variant="h5">
                            "Plan Your Meetings!"
                        </Typography>
                        <form className={this.props.classes.form} onSubmit={this.handleSubmit}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input id="email" name="email" autoComplete="email" autoFocus value={this.state.email} onChange={this.handleInputChange} />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Name</InputLabel>
                                <Input name="name" type="string" id="username" value={this.state.name} onChange={this.handleInputChange}/>
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={this.props.classes.submit}
                            >
                                Sign in
                            </Button>
                        </form>
                    </Paper>
                </main>
            );
        }

    }


}


SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);