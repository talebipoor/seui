import { Form, Text } from 'react-form';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';


export default class Options extends Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    submitOptions = ()=>{
        alert(this.state.submittedValues.options[0])
        axios.post('http://localhost:8000/polls/addoption', {
            body: {
                id:this.props.poll,
                options:this.state.submittedValues.options
            },
            headers:{
                "user":"username:ali"
            },
            withCredentials: true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    alert(data)
                }
            })

    }

    render() {
        return (
            <div style={{marginBottom:20, marginTop:20}}>
                <Form
                    onSubmit={
                        (submittedValues) => {this.setState({submittedValues: submittedValues});
                            this.submitOptions()
                        }
                    }>
                    {formApi=>(
                        <div>
                            <Button
                                style={{marginTop:3}}
                                variant="contained"
                                color="primary"
                                onClick={() => formApi.addValue('options', '')}
                                type="button"
                                className="mb-4 mr-4 btn btn-success">Add Option</Button>
                            <form onSubmit={formApi.submitForm} id="dynamic-form">
                                { formApi.values.options && formApi.values.options.map( ( option, i ) => (
                                    <div key={'option${i}'}>
                                        <label htmlFor={'option-name-${i}'}></label>
                                        <Text label="option" field={['options', i]} id={'option-name-${i}'} />
                                        <IconButton aria-label="Delete"
                                                    onClick={() => formApi.removeValue('options', i)}
                                                    type="button"
                                                    className="mb-4 btn btn-danger">
                                            <DeleteIcon fontSize="small" />
                                        </IconButton>
                                    </div>
                                ))}
                                <Button style={{marginTop:3}}  variant="contained" color="secondary" type="submit" className="mb-4 btn btn-primary">Confirm Options</Button>
                            </form>
                        </div>
                    )}
                </Form>
            </div>
        );
    }
}