/**
 * Created by Saghar on 12/24/18.
 */

import React, { Component } from 'react';
import ButtonAppBar from "../Features/ButtonAppBar"
import UserPolls from "../Features/UserPolls"
import withStyles from '@material-ui/core/styles/withStyles';
import axios from 'axios';
import Cookie from 'js-cookie'

const styles = theme => ({
    main: {
    }
});

class Polls extends Component {
    constructor(props) {
        super(props);
        this.state={
          user:{}
        };

    }

    componentWillMount(){
        this.getLoggedInUser();
    }


    getLoggedInUser(){
        // var user
        // axios.get('http://localhost:8000/polls/login')
        //     .then((res, err) => {
        //         if(err){
        //             alert(err)
        //         }else{
        //             const data = res.data;
        //             alert(data)
        //             user = {
        //                 name:data.name,
        //                 email:data.email
        //             }
        //         }
        //     })
        // user = {
        //     name:"Saghar Talebipour",
        //     email:"saghar.talebipoor@gmail.com"
        // }
        //
        // this.setState({
        //     user:user
        // });
    }

    render(){
        return (
            <div>
                <ButtonAppBar username={Cookie.get('username')}/>
                <UserPolls/>
            </div>
        );
    }
}

export default  withStyles(styles)(Polls);
