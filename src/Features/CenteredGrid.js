import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InvitedPoll from './InvitedPoll'
import CreatedPoll from './CreatedPoll'
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

function CenteredGrid(props) {
    const { classes } = props;

    return (
        <div className={classes.root}>
            <Grid container spacing={24}>
                <Grid item xs={6}>
                    <h1 align="center">Created Polls</h1>
                    {props.polls.createdPolls.map(createdPoll => (
                        <CreatedPoll poll={createdPoll}/>
                    ))}
                </Grid>
                <Grid item xs={6}>
                    <h1 align="center">Invited Polls</h1>
                    {props.polls.invitedPolls.map(invitedPoll => (
                        <InvitedPoll poll={invitedPoll}/>
                    ))}
                </Grid>
            </Grid>
        </div>
    );
}

CenteredGrid.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);