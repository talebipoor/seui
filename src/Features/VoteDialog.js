/**
 * Created by Saghar on 12/24/18.
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import axios from 'axios';
import CommentIcon from '@material-ui/icons/Comment';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';



const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
    },
    root: {
        color: green[600],
        '&$checked': {
            color: green[500],
        },
    },
    checked: {},

});

const options=[{
    id:"1234567",
    text:"Thursday afternoon at 18"
},{
    id:"7654321",
    text:"Friday morning at 9"
}
]


export default class VoteDialog extends React.Component {

    state = {
            open: false,
            commentOpen: false,
            options: [],
            choices:[],
            selectedValue:[],
            comments:[],
            optionId:0,
            comment:"",
            finalOption:{}
    };

    handleCancel = () =>{
        this.setState({open:false})
    }

    handleCommentClose = ()=>{
        this.setState({commentOpen:false})
    }

    handleClickOpen = () => {
        this.getPollOptions(this.props.pollId)
        this.setState({ open: true });
    };

    getOption = (id) => {
        var data={}
        axios.get('http://localhost:8000/polls/option', {
            params: {
                optionId: id
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        }).then((res, err) =>{
            if(err){
                alert(err)
            }else{
                data = res.data
                this.setState({
                    finalOption:data
                });
            }
        });
        this.setState({
            finalOption:data
        });
    }

    handleResult = () => {
        this.getOption(this.props.status)
        this.setState({ open: true})
    }

    getPollOptions = (pollId) => {
        var options=[]
        axios.get('http://localhost:8000/polls/options',{
            params: {
                pollId:pollId
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    // alert(JSON.stringify(data))
                    this.setState({
                        options:data
                    });
                }
            })
        this.setState({
           options:options
        });
    }

    handleSubmit = () => {
        axios.post('http://localhost:8000/polls/vote', {
            body: {
                pollId:this.props.pollId,
                choices:this.state.choices
            },
            header:{
                Cookie:"username:ali"
            },
            withCredentials:true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    if(data.overlap){
                        alert(data.overlap)
                    }
                }
            })
        this.setState({ open: false });
    };

    handleYesVote = (event, optionId) => {
        var newChoices = this.state.choices
        var selectedValue = this.state.selectedValue
        var index = newChoices.findIndex(x => x.id==optionId)
        var c = {
            optionId: optionId,
            answer: 1
        }
        if(index==-1){
            newChoices.push(c)
        }
        else{
            newChoices[index]=c;
        }
        selectedValue[optionId]=event.target.value
        this.setState({
            choices: newChoices,
            selectedValue: selectedValue
        })
    };

    handleNoVote = (event, optionId) => {
        var newChoices = this.state.choices
        var selectedValue = this.state.selectedValue
        var index = newChoices.findIndex(x => x.id==optionId)
        var c = {
            optionId: optionId,
            answer: 2
        }
        if(index==-1){
            newChoices.push(c)
        }
        else{
            newChoices[index]=c;
        }
        selectedValue[optionId]=event.target.value
        this.setState({
            choices: newChoices,
            selectedValue : selectedValue
        })
    };

    showComments = (optionId) => {
        this.setState({
            commentOpen:true,
            optionId:optionId
        })
        var comments = []
        axios.get('http://localhost:8000/polls/comments', {
            params: {
                pollId:this.props.pollId,
                optionId:optionId
            },headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    alert(JSON.stringify(data))
                    comments = data.comments
                    this.setState({comments:comments})
                }
            })
        this.setState({comments:comments})
    }

    handleOnlyVote = (event, optionId) => {
        var newChoices = this.state.choices
        var selectedValue = this.state.selectedValue
        var index = newChoices.findIndex(x => x.id==optionId)
        var c = {
            optionId: optionId,
            answer: 3
        }
        if(index==-1){
            newChoices.push(c)
        }
        else{
            newChoices[index]=c;
        }
        selectedValue[optionId]=event.target.value
        this.setState({
            choices: newChoices,
            selectedValue : selectedValue
        })
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        this.setState({
            comment: value
        });
    }

    handleAddComment = ()=>{
        var comment = this.state.comment
        axios.post('http://localhost:8000/polls/saveComment', {
            body: {
                pollId:this.props.pollId,
                optionId:this.state.optionId,
                comment_text:comment
            },
            headers:{
                "user":"username:ali"
            },
            withCredentials: true

        })
            .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    alert(JSON.stringify(data))
                    this.setState({
                        comment:""
                    })
                    this.showComments(this.state.optionId)
                }
            })


    }


    render($) {



        return (
            <div>
                <Button styles={{marginRight:20}} variant="contained" color="secondary" disabled={this.props.status} onClick={this.handleClickOpen}>
                    vote
                </Button>
                <Button styles={{paddingLeft:200}} variant="contained" disabled={!this.props.status} color="primary" onClick={this.handleResult}>
                    View Results
                </Button>
                <Dialog
                    open={this.state.open && !this.props.status}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    style={{width:2000}}
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">vote</DialogTitle>
                    <DialogContent>
                        <List>
                            {this.state.options.map(opt => (
                                <ListItem>
                                    <ListItemText
                                        primary={opt.text}
                                    />
                                    <div>
                                    <ListItemSecondaryAction>
                                        <FormControlLabel
                                            checked={this.state.selectedValue[opt.id] === 'a'}
                                            onChange={(e) => this.handleYesVote(e, opt.id)}
                                            value="a"
                                            name="radio-button-demo"
                                            control={<Radio />}
                                            aria-label="A"
                                            label="Yes"
                                            labelPlacement="start"

                                        />
                                        <FormControlLabel
                                            checked={this.state.selectedValue[opt.id] === 'b'}
                                            onChange={(e) => this.handleNoVote(e, opt.id)}
                                            value="b"
                                            control={<Radio />}
                                            name="radio-button-demo"
                                            aria-label="B"
                                            label="No"
                                            labelPlacement="start"

                                        />
                                        <FormControlLabel
                                            checked={this.state.selectedValue[opt.id] === 'c'}
                                            onChange={(e) => this.handleOnlyVote(e, opt.id)}
                                            value="c"
                                            control={<Radio />}
                                            name="radio-button-demo"
                                            aria-label="C"
                                            label="Only if I have to"
                                            labelPlacement="start"

                                        />
                                        <IconButton
                                            onClick={()=>this.showComments(opt.id)}
                                            aria-label="Comments">
                                            <CommentIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                    </div>
                                </ListItem>
                            ))}


                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} variant="contained" color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} variant="contained" color="secondary">
                            Submit Vote
                        </Button>
                    </DialogActions>

                </Dialog>
                <Dialog
                    open={this.state.open && this.props.status}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    style={{width:2000}}
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">Final Result</DialogTitle>
                    <DialogContent>
                        <List>
                                <ListItem>
                                    Final option is: {this.state.finalOption.text}
                                </ListItem>
                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} variant="contained" color="primary">
                            Close
                        </Button>
                    </DialogActions>

                </Dialog>
                <Dialog
                    open={this.state.commentOpen}
                    onClose={this.handleCommentClose}
                    aria-labelledby="form-dialog-title"
                    style={{width:2000}}
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">Comments</DialogTitle>
                    <DialogContent>
                        <List>
                            {this.state.comments.map(comment => (
                                <ListItem>
                                    <ListItemText
                                        primary={comment.user + " said: --- "+comment.text}
                                    />
                                </ListItem>
                            ))}
                            <ListItem>
                                <TextField
                                    id="outlined-full-width"
                                    label="comment"
                                    style={{ margin: 8 }}
                                    placeholder="Enter your comment here."
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.comment}
                                    onChange={this.handleChange}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <Button onClick={this.handleAddComment} variant="contained" color="secondary">
                                    Add Comment
                                </Button>
                            </ListItem>
                        </List>
                    </DialogContent>
                    <DialogActions>

                        <Button onClick={this.handleCommentClose} variant="contained" color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}