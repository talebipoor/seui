/**
 * Created by Saghar on 12/24/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import FormDialog from './FormDialog'
import AddIcon from '@material-ui/icons/Add';
import { Redirect } from 'react-router-dom'



const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});



class ButtonAppBar extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
        };
    }


    handleLogout = () =>{
        this.setState({
            redirect:true
        })
    }

    render(props) {
            if(this.state.redirect){
                return(<Redirect to={"/login" +
                ""}/>)
            }
            else{
                return(<div className={this.props.classes.root}>
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="h6" color="inherit" className={this.props.classes.grow}>
                                {this.props.username}
                            </Typography>
                            <FormDialog className={this.props.classes.button}/>
                            <Button variant="contained" color="secondary" onClick={this.handleLogout}>
                                Log out
                            </Button>
                        </Toolbar>
                    </AppBar>
                </div>)
            }

    }
}

ButtonAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);