/**
 * Created by Saghar on 12/24/18.
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
    },
    primary:{
        width: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
    }
});


export default class ResultDialog extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            finalize: false,
            finalOption:{},
            choices:[],
            msg:""
        };
    }

    componentWillMount(){
    }

    getOption = (id) => {
        var data={}
        axios.get('http://localhost:8000/polls/option', {
            params: {
                optionId: id
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        }).then((res, err) =>{
            if(err){
                alert(err)
            }else{
                data = res.data
                this.setState({
                    finalOption:data
                });
            }
        });
        this.setState({
            finalOption:data
        });
    }

    getMyPoll = (pollId) => {
        var choices=[]
        var final = 0
        axios.get('http://localhost:8000/polls/result', {
            params: {
                pollId: this.props.pollId
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        })
         .then((res, err) => {
                if(err){
                    alert(err)
                }else{
                    const data = res.data;
                    alert(JSON.stringify(data))
                    choices = data.optionChoiceList
                    final = data.status
                    if(final!=0){
                        this.getOption(final)
                    }
                    this.setState({
                        choices:choices,
                        finalize: final
                    })
                }
            })
        this.setState({
            finalize:final,
            choices:choices

        });
    };

    handleClickOpen = () => {
        this.getMyPoll(this.props.pollId)
        this.setState({ open: true });

    };

    handleClosePoll = () => {
        this.setState({open: false})
    };

    handleFinalResult = (opt) => {
        axios.post('http://localhost:8000/polls/finalize',{
            body:{
                pollId: this.props.pollId,
                optionId: opt.id,
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        }).then((res, err)=>{
            if(err){
                alert(err)
            }else{
                const data = res.data;
                alert(data)
            }
        })
        this.setState({
            finalize: true,
            finalOption: opt
        });
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        this.setState({
            msg: value
        });
    }

    handleRevoke = () => {
        axios.post('http://localhost:8000/polls/revoke',{
            body:{
                pollId: this.props.pollId,
                msg:this.state.msg
            },
            headers:{
                Cookie:"username:ali"
            },
            withCredentials: true
        }).then((res, err)=>{
            if(err){
                alert(err)
            }else{
                const data = res.data;
                alert(data)
            }
        })
        this.setState({
            msg:"",
            finalize: false,
        });
    }



    render($) {
        return (
            <div>
                <Button variant="contained" color="primary" onClick={this.handleClickOpen}>
                    View Results and Close Poll
                </Button>
                <Dialog
                    open={this.state.open && !this.state.finalize}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    style={{width:750}}
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">Poll Results</DialogTitle>
                    <DialogContent>
                        <List>
                            {this.state.choices.map(opt => (
                                <ListItem>
                                    <List>
                                        <ListItem>
                                            <ListItemText
                                                primary={opt.text + " :  -Yes Count: "+ opt.selectors.length + " :  -No Count: "+ opt.rejectors.length }
                                            />
                                        </ListItem>

                                        <ListItem>
                                            <ListItemText primary={"selectors: "}/>
                                            {opt.selectors.map(selector =>(<ListItemText primary={selector.name} />))}
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText primary={"rejectors:"}/>
                                            {opt.rejectors.map(rejector =>(<ListItemText primary={rejector.name} />))}
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText
                                                disableTypography
                                                primary={<Typography type="body2" style={{ color: '#FF0000' }}>Only if I have to</Typography>}
                                            />
                                            {opt.maybe.map(maybe =>( <ListItemText
                                                disableTypography
                                                primary={<Typography type="body2" style={{ color: '#FF0000' }}>{maybe.name}</Typography>}
                                            />))}
                                        </ListItem>
                                    </List>
                                    <ListItemSecondaryAction style={{float:'left'}}>
                                        <Button fullWidth variant="contained" color="secondary" onClick={() => this.handleFinalResult(opt)}>
                                            Choose as final result
                                        </Button>
                                    </ListItemSecondaryAction>
                                </ListItem>

                            ))}


                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClosePoll} variant="contained" color="primary">
                            Close
                        </Button>
                    </DialogActions>

                </Dialog>
                <Dialog
                    open={this.state.open && this.state.finalize}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    style={{width:750}}
                    fullWidth
                >
                    <DialogTitle id="form-dialog-title">Finalized Poll Results</DialogTitle>
                    <DialogContent>
                        <List>
                            <ListItem>
                                Finalized Option is: {this.state.finalOption.text}
                            </ListItem>
                            <ListItem>
                            <TextField
                                id="outlined-full-width"
                                label="Revoke message"
                                style={{ margin: 8 }}
                                placeholder="If left empty the default notification message will be sent."
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                value={this.state.msg}
                                onChange={this.handleChange}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            </ListItem>
                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClosePoll} variant="contained" color="primary">
                            Close
                        </Button>
                        <Button onClick={this.handleRevoke} variant="contained" color="secondary">
                            Revoke Poll
                        </Button>
                    </DialogActions>

                </Dialog>
            </div>
        );
    }
}