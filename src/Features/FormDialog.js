/**
 * Created by Saghar on 12/24/18.
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import Options from './Options'
import InvitedPeople from './InvitedPeople'
import axios from 'axios';
import Cookies from 'js-cookie';


export default class FormDialog extends React.Component {
    state = {
        open: false,
        pollCreated: false,
        name:'',
        description:'',
        pollId:0
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    }

    handleSubmit = () => {
        if(this.state.pollCreated){
            this.setState({ pollCreated: false})
            this.setState({ open: false });
        }
        else{
            axios.post('http://localhost:8000/polls/create', {
                headers:{
                    "user":"username:ali"
                },
                body: {
                    name:this.state.name,
                    description:this.state.description
                }
            })
                .then((res, err) => {
                    if(err){
                        alert(err)
                    }else{
                        const data = res.data;
                        alert(data)
                        this.setState({
                            pollId:data.pollId
                        });
                    }
                })
            this.setState({ pollCreated: true})
        }
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    render($){
        return(
            <div style={{paddingRight:10}}>
                <Button variant="contained" color="secondary" onClick={this.handleClickOpen}>
                    <AddIcon style={{fontSize: 15}} />
                    Create New Poll
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Create New Poll</DialogTitle>
                    <DialogContent>
                        <TextField
                            value={this.state.name}
                            onChange={this.handleChange}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Poll Name"
                            type="string"
                            name="name"
                            fullWidth
                        />
                        <TextField
                            value={this.state.description}
                            onChange={this.handleChange}
                            autoFocus
                            margin="dense"
                            id="description"
                            name="description"
                            label="Poll Description"
                            type="string"
                            fullWidth
                        />
                        { this.state.pollCreated && <Options poll={this.state.pollId}/> }
                        { this.state.pollCreated && <InvitedPeople poll={this.state.pollId} /> }
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Create Poll
                        </Button>
                    </DialogActions>

                </Dialog>
            </div>
        );
    }
}